import PropTypes from 'prop-types';
import React from 'react';

const ProductList = ({products, onProductSelect}) => (
    <ul>{products.map(product =>
        (<li onClick={() => onProductSelect(product)} key={product.id}>
            {product.name} {product.brand}
        </li>))}
    </ul>
);

ProductList.propTypes = {
    products: PropTypes.array.isRequired,
    onProductSelect: PropTypes.func.isRequired
};


export default ProductList;
